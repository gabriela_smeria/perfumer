<?php
include "pagination.php";
include "config/config.php";


header('Content-Type: text/xml');
$siteroot = '/perfumer';

$searchExpr = $_GET['searchName'].'%';

$stmt_count = $db->prepare('SELECT count(*) as numberofrecords FROM products WHERE name COLLATE UTF8_GENERAL_CI LIKE :searchexpr ORDER BY name');
$stmt_count->bindParam(':searchexpr', $searchExpr, PDO::PARAM_STR);
$stmt_count->execute();

$row = $stmt_count->fetch(PDO::FETCH_ASSOC);
$totalRecords = intval($row['numberofrecords']);

$stmt = $db->prepare('SELECT productID, image, name, brand, sex, price, promo FROM products WHERE name COLLATE UTF8_GENERAL_CI LIKE :searchexpr ORDER BY name LIMIT :itemsPerPage OFFSET :startat');
$stmt->bindParam(':searchexpr', $searchExpr, PDO::PARAM_STR);
$stmt->bindParam(':itemsPerPage', $totalRecords, PDO::PARAM_INT);
$stmt->bindParam(':startat', $offset, PDO::PARAM_INT);
$stmt->execute();
echo '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>';
echo '<response>';
echo "<totalNumber>".$totalRecords."</totalNumber>";
echo "<currentPage>".$page."</currentPage>";
echo "<productsList><ul class=\"product_item\">";
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
  echo '<li id="'.$row['productID'].'">';
  if ($user->is_logged_in() ) {
      if($_SESSION['username'] == 'admin') {
          echo '<img onclick=removeFromHomepage("'.$siteroot. '/removeProductService.php?deletedProductID='.$row['productID'].'",' .$row['productID']. ')'.' id="icons" src="https://cdn2.iconfinder.com/data/icons/windows-8-metro-style/512/delete.png" alt="delete" width=20 height=20>';
      }
  }
	echo '<a href="perfumepage.php?productID='.$row['productID'].'">';
	if($row['promo'] > 0){
		echo '<span class="new"><img src="'.$siteroot.'/images/elements/sale.png" alt="Promotii" /></span>';
	}
		echo '<img src="'.$siteroot.'/images/perfumes/'.$row['brand'].'/'.$row['image'].'.jpg" alt="Perfumer" width="155" heigth="155" />';
		echo '<span class="name">'.$row['name'].'</span>';
		echo '<span class="brand">by '.$row['brand'].'</span>';
		echo '<span class="type">';
		if($row['sex'] != 'unisex')
		echo ' pentru ';
		echo $row['sex'].'</span>';
		if($row['promo'] > 0){
		echo '<span class="oldprice">'.$row['price'].' RON pret vechi</span>';
		$newprice = $row['price'] - $row['price']*$row['promo']/100;
		echo '<span class="price">'.ceil($newprice).' RON</span>';
		}
		else{
			echo '<span class="price">'.$row['price'].' RON</span>';
		}
	echo '</a>';
	if($user->is_logged_in()){
      echo '<button class="add-to-cart-btn bottom-right-corner" onclick="addToCart('.$row['productID'].')"></button>';
  } else {
      echo '<button class="add-to-cart-btn bottom-right-corner" onclick="window.location = \''.$siteroot.'/loginpage.php\';"></button>';
  }
	echo '</li>';
}
echo "</ul></productsList>";
echo "</response>";
?>
