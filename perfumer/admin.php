<?php include 'includes/head.php'; ?>
<?php include 'includes/pagetop.php'; ?>
<div id="site">
  <div id="content">
      <div id="wrapper">
            <div>
                <div class="filters">
                  <label class="">
                     Selectie
                    </label>
                  <form method="get" id="fltForm">
                      <div>
                      <label class="select_label">
                            <select id="category_select" onchange="showResults()" name="category">
                                <option value="-1"> Categorie </option>
                                <option value="barbati">Parfumuri pentru barbati</option>
                                <option value="femei">Parfumuri pentru femei</option>
                                <option value="unisex">Parfumuri unisex</option>

                             </select>
                         </label>
                         <label class="select_label">
                            <select id="type_select" onchange="showResults()" name="type">
                                <option value="-1"> Tip </option>
                                <option value="deodorant">Dedorant</option>
                                <option value="set cadou">Set cadou</option>

                             </select>
                         </label>
                         </div>
                         <div>
                         <label class="select_label">
                             <select id="essence_select" onchange="showResults()" name="essence">
                                 <option value="-1"> Esenta</option>
                                 <option value="female" disabled>Femei -- </option>
                                 <?php
                                 try {
                                    $stmt = $db->prepare('SELECT name, flavours FROM essences WHERE sex = "female" ORDER BY name');
                                    $stmt->execute();
                                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                        echo '<option value="'.$row['name'].'">'.$row['name'].' - '.$row['flavours'].'</option>';

                                    }
                                    } catch(PDOException $e) {
                                    echo '<p class="bg-danger">'.$e->getMessage().'</p>';
                                    }
                                 ?>
                                 <option value="male" disabled>Barbati -- </option>
                                 <?php
                                 try {
                                    $stmt = $db->prepare('SELECT name, flavours FROM essences WHERE sex = "male" ORDER BY name');
                                    $stmt->execute();
                                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                        echo '<option value="'.$row['name'].'">'.$row['name'].' - '.$row['flavours'].'</option>';

                                    }
                                    } catch(PDOException $e) {
                                    echo '<p class="bg-danger">'.$e->getMessage().'</p>';
                                    }
                                 ?>
                             </select>
                         </label>
                         <label class="select_label">
                          <select id="pricerange_select" onchange="showResults()" name="pricerange">
                              <option value="-1"> Pret</option>
                                <option value="0"> 0-100 RON </option>
                                <option value="100"> 100-200 RON </option>
                                <option value="200"> 200-300 RON </option>
                                <option value="300"> 300-400 RON </option>
                                <option value="400"> 400-500 RON </option>
                            </select>
                         </label>
                         </div>
                    </form>
                </div>
                <br>
              <table>
                <thead>
                    <tr>
                    <th>Product ID</th>
                    <th>Name</th>
                    <th>Brand</th>
                    <th>Type</th>
                    <th>Essence</th>
                    <th>Sex</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Promo</th>
                    <th>Date</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody id="tableResults">
                      <?php
                      $stmt = $db->prepare('SELECT productID, name, brand, sex, price, promo, type, essence, quantity, date FROM products ORDER BY name');
                      $stmt->execute();

                      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                      echo '<tr>';
                      echo '<td>' .$row['productID']. '</td><td>' .$row['name']. '</td><td>' .$row['brand']. '</td><td>' .$row['type']. '</td><td>' .$row['essence']. '</td><td>' .$row['sex']. '</td><td>' .$row['price']. '</td><td>' .$row['quantity']. '</td><td>' .$row['promo']. '</td><td>' .$row['date']. '<td><a href="#" onclick=callAjax("'.$siteroot. '/removeProductService.php?deletedProductID='.$row['productID'].'",' .$row['productID']. ')';
                      echo '><img id="icons" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTyktkVIiVibyE-ZZiabihk9xyspFxePqhmWHzXK9JTwEXGrCzt8VdRK9I" alt="delete" width=20 height=20></a></td>';
                      echo '</tr>';
                      }
                    ?>
                      <tr><td><br><a href=<?php echo $siteroot.'/exportToCSV.php?csv=1'?>><button id="exportButton">Export to CSV</button></a></td></tr>
                </tbody>

              </table>
            </div>
      </div>
  </div>
</div>



<?php include 'includes/pagebottom.php'; ?>
