var xmlHttp = createXmlHttpRequestObject();

function createXmlHttpRequestObject(){
	var xmlHttp;
	if(window.ActiveXObject){
		try{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(e){
			xmlHttp=false;
		}
	}
	else{
		try{
			xmlHttp = new XMLHttpRequest();
		}catch(e){
			xmlHttp=false;
		}
	}
	if(!xmlHttp)
		alert('impossible');
	else return xmlHttp;

}

function addToCart(productId){
        var quantity = document.getElementById('prod-quantity');
        if(quantity){
          quantity = encodeURIComponent(quantity.value);
        }
        else{
          quantity = 1;
        }
         if(xmlHttp.readyState==4 || xmlHttp.readyState==0){
                  xmlHttp.onreadystatechange = handleServerResponseAddToCart;
		              xmlHttp.open("GET","add_product_to_cart.php?productid="+productId+"&quantity="+quantity,true);
                  xmlHttp.setRequestHeader('Content-Type',  'text/xml');
                  xmlHttp.send(null);
         }else{
               setTimeout('addToCart('+productId+')',1000);
         }
         event.preventDefault();
         event.stopPropagation();
}

function handleServerResponseAddToCart(){
    if(xmlHttp.readyState==4){
		  if(xmlHttp.status==200){
			   var xmlResponse = xmlHttp.responseXML;
			   var xmlDocumentElement = xmlResponse.documentElement;
         var productAdded = xmlDocumentElement.getElementsByTagName("added")[0].innerHTML;
         if(productAdded > 0){
			     alert("Produsul a fost adaugat in cos!");
       }else{
        alert("Stoc insuficient!");
       }
      }
      else{
        alert(xmlHttp.status);
      }
}
}
