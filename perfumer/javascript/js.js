// JavaScript Document

function changeType(type){
	document.getElementById('type_select').value=type;
	document.getElementById('category_select').value="-1";
	showResults();
}
function changeCategory(category){
	document.getElementById('category_select').value=category;
	document.getElementById('type_select').value="-1";
	showResults();
}
function showResults(){

	var category = document.getElementById('category_select').value;
	var type = document.getElementById('type_select').value;
	var essence = document.getElementById('essence_select').value;
	var pricerange = document.getElementById('pricerange_select').value;
	var ok=0;

	if(window.location.pathname == '/perfumer/admin.php') {
		ok=1;
	}

	if (ok) {
		document.getElementById("tableResults").innerHTML="";
	} else {
		document.getElementById("products").innerHTML="";
	}

	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {  // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			if(ok) {
				document.getElementById("tableResults").innerHTML=xmlhttp.responseText;
			} else {
				document.getElementById("products").innerHTML=xmlhttp.responseText;
			}
		}
	}
	var results;
	if(ok) {
		results = 'getResultsAdmin.php';
	} else {
		results = 'getResults.php';
	}
	xmlhttp.open("GET",results+window.location.search+"&category="+category+"&type="+type+"&essence="+essence+"&pricerange="+pricerange,true);
	xmlhttp.send();

}
function callAjax(url, objNr){
    var xmlhttp;
    // compatible with IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function(){
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
        		var a = document.querySelectorAll("tbody tr td:first-child");
        		for (i=0; i<a.length; i++){
        			if(objNr==a[i].innerHTML) {
        				a[i].parentNode.parentNode.removeChild(a[i].parentNode);
        			}
        		}
        }
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function removeFromHomepage(url, objNr){
    var xmlhttp;
    // compatible with IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function(){
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
        		var a = document.getElementById(objNr);
        		a.parentNode.removeChild(a);
        }
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}


