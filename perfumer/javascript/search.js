var xmlHttp = createXmlHttpRequestObject();

function createXmlHttpRequestObject(){
	var xmlHttp;
	if(window.ActiveXObject){
		try{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(e){
			xmlHttp=false;
		}
	}
	else{
		try{
			xmlHttp = new XMLHttpRequest();
		}catch(e){
			xmlHttp=false;
		}
	}
	if(!xmlHttp)
		alert('impossible');
	else return xmlHttp;

}

function search(page){
         if(xmlHttp.readyState==4 || xmlHttp.readyState==0){
                  xmlHttp.onreadystatechange = handleServerResponseSearch;
		              var searchName = encodeURIComponent(document.getElementById("quickSearch").value);
                  if (searchName) {
                    xmlHttp.open("GET","search.php?searchName="+searchName+"&page"+page+"&items=8",true);
                    xmlHttp.setRequestHeader('Content-Type',  'text/xml');
                    xmlHttp.send(null);
                }

         }else{
               setTimeout('search('+page+')',1000);
         }
}

function handleServerResponseSearch(){
    if(xmlHttp.readyState==4){
		  if(xmlHttp.status==200){ //daca sesiunea de com e ok
			   var xmlResponse = xmlHttp.responseXML;
			   var xmlDocumentElement = xmlResponse.documentElement;
         var totalNumber = xmlDocumentElement.getElementsByTagName("totalNumber")[0].innerHTML;
         var elementtofill = document.getElementById("products");
         if(!elementtofill){
            elementtofill = document.getElementsByClassName("right_siderbar")[0];
            elementtofill.innerHTML = '';
            var node = document.createElement("H1");                 // Create a <li> node
            var textnode = document.createTextNode("Rezultatele cautarii");         // Create a text node
            node.appendChild(textnode);
            elementtofill.appendChild(node);
            var productsNewElement = document.createElement("DIV");
            productsNewElement.setAttribute("id", "products");
            elementtofill.appendChild(productsNewElement);
            elementtofill = productsNewElement;
         }else{
            var titleH = document.getElementsByTagName("h1")[0];
            titleH.innerHTML="Rezultatele cautarii";
         }

         if(totalNumber>0){
			   var products = (xmlDocumentElement.getElementsByTagName("productsList"))[0];
         var currentPage = parseInt(xmlDocumentElement.getElementsByTagName("currentPage")[0].innerHTML);
         var lastPage = Math.ceil(totalNumber/8);
         var productsContent =  products.innerHTML;
         var nextPage = 0;
   /*       if(currentPage>1){
            nextPage = currentPage-1;
  productsContent += "<a onclick=\"search("+nextPage+")\">prev</a>"+
  " <a onclick=\"search(1)\">1</a> ...";
}

productsContent+= "<a class=\"current-page\">"+currentPage+"</a> ";

if(currentPage < lastPage){
  nextPage = currentPage + 1;
  productsContent += "... "+
   "<a onclick=\"search("+lastPage+")\">"+lastPage+"</a> " +
   "<button onclick=\"search("+nextPage+")\">next</button>";
}*/
         elementtofill.innerHTML = productsContent;
       }
       else {
          elementtofill.innerHTML = "<p>Niciun rezultat!</p>";
       }
      }
      else{
        alert(xmlHttp.status);
      }
}
}
