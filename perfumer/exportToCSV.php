<?php
include 'config/config.php';

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=data.csv');

$output = fopen('php://output', 'w');
if($_GET["csv"] == 1){

  fputcsv($output, array('ID', 'Name', 'Brand', 'Sex', 'Price', 'Promo', 'Type', 'Essence', 'Quantity', 'Date'));

  $stmt = $db->prepare('SELECT productID, name, brand, sex, price, promo, type, essence, quantity, date FROM products ORDER BY name');
  $stmt->execute();

} else {
$category = $_GET['category'];
$type = $_GET['type'];
$essence = $_GET['essence'];
$pricelower = $_GET['pricerange'];
$priceupper = $pricelower+100;

fputcsv($output, array('ID', 'Name', 'Brand', 'Sex', 'Price', 'Promo', 'Type', 'Essence', 'Quantity', 'Date'));

$stmt = $db->prepare('SELECT productID, name, brand, sex, price, promo, type, essence, quantity, date FROM products WHERE (CASE WHEN :sex <> -1 THEN sex = :sex ELSE 1=1  END) AND (CASE WHEN :type <> -1 THEN type = :type ELSE 1=1  END) AND (CASE WHEN :essence <> -1 THEN essence = :essence ELSE 1=1  END) AND (CASE WHEN :pricelower <> -1 THEN price > :pricelower AND price <= :priceupper ELSE 1=1  END) ');
$stmt->execute(array('sex' => $category, 'type' => $type, 'essence' => $essence, 'pricelower' => $pricelower, 'priceupper' => $priceupper));

}
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    fputcsv($output, $row);
  }
?>
