<?php
class Cos {

    private $_db;

    function __construct($db){

    	$this->_db = $db;
    }

	public function getContentForUser($username){

		try {
			$stmt = $this->_db->prepare('SELECT p.productID, p.image, p.name, p.brand, p.sex, p.price, p.promo, o.quantity FROM orderlines as o JOIN products as p ON o.productid=p.productid WHERE username = :username AND orderid = -1');
			$stmt->execute(array('username' => $username));

			$cart=array();
			$i=0;
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$cart[$i] = $row;
				$i++;
			}
			return $cart;

		} catch(PDOException $e) {
		    echo '<p class="bg-danger">'.$e->getMessage().'</p>';
		}
	}

	public function addProduct($username,$productId,$number){

		try {

			$stmt = $this->_db->prepare('SELECT p.quantity as stoc, o.quantity as quant FROM (select productid, quantity from orderlines where username = :username and orderid = -1 and productid = :productid) o right join (select productID, quantity from products WHERE productID = :productid) p on o.productid = p.productID');
			$stmt->bindParam(':username', $username, PDO::PARAM_STR);
			$stmt->bindParam(':productid', $productId, PDO::PARAM_INT);

			$stmt->execute();
			$numberFromDB = $stmt->fetch(PDO::FETCH_ASSOC);
			if($numberFromDB){
				$productStoc = $numberFromDB['stoc'];
				$numberFromDB = intval($numberFromDB['quant']);
				$productStoc = intval($productStoc);
				if($productStoc >= $number + $numberFromDB){
					if($numberFromDB > 0 ) {
						$this->changeProduct($username,$productId,$numberFromDB+$number);
					} else {
						$productId = intval($productId);
						$number = intval($number);
						$stmt1 = $this->_db->prepare('INSERT INTO orderlines (productID, quantity, username) VALUES (:productid, :quantity, :username) ');
						$stmt1->bindParam(':username', $username, PDO::PARAM_STR);
						$stmt1->bindParam(':productid', $productId, PDO::PARAM_INT);
						$stmt1->bindParam(':quantity', $number, PDO::PARAM_INT);

						$stmt1->execute();
					}
				}else{
					return false;
				}
			}

		} catch(PDOException $e) {
		    echo '<p class="bg-danger">'.$e->getMessage().'</p>';
		    return false;
		}
		return true;
	}

	public function removeProduct($username,$productId){

		try {
			$stmt = $this->_db->prepare('DELETE FROM orderlines WHERE productid = :productid AND username = :username AND orderid = -1 ');
			$stmt->bindParam(':username', $username, PDO::PARAM_STR);
			$stmt->bindParam(':productid', $productId, PDO::PARAM_INT);

			$stmt->execute();


		} catch(PDOException $e) {
		    echo '<p class="bg-danger">'.$e->getMessage().'</p>';
		    return false;
		}
		return true;
	}

	public function changeProduct($username,$productId,$number){
		try {
			$productId = intval($productId);
			$number = intval($number);

			$stmt = $this->_db->prepare('SELECT p.quantity as stoc, o.quantity as quant FROM (select productid, quantity from orderlines where username = :username and orderid = -1 and productid = :productid) o right join (select productID, quantity from products WHERE productID = :productid) p on o.productid = p.productID');
			$stmt->bindParam(':username', $username, PDO::PARAM_STR);
			$stmt->bindParam(':productid', $productId, PDO::PARAM_INT);

			$stmt->execute();
			$numberFromDB = $stmt->fetch(PDO::FETCH_ASSOC);
			$productStoc = $numberFromDB['stoc'];

			if($productStoc >= $number){

				$stmt = $this->_db->prepare('UPDATE orderlines SET quantity = :quantity WHERE productid = :productid AND username = :username AND orderid = -1 ');
				$stmt->bindParam(':username', $username, PDO::PARAM_STR);
				$stmt->bindParam(':productid', $productId, PDO::PARAM_INT);
				$stmt->bindParam(':quantity', $number, PDO::PARAM_INT);

				$stmt->execute();
			} else
			{
				return false;
			}

		} catch(PDOException $e) {
		    echo '<p class="bg-danger">'.$e->getMessage().'</p>';
		    return false;
		}
		return true;
	}

	public function emptyContent($username){
		try {
			$stmt = $this->_db->prepare('DELETE FROM orderlines WHERE username = :username AND orderid = -1 ');
			$stmt->bindParam(':username', $username, PDO::PARAM_STR);

			$stmt->execute();


		} catch(PDOException $e) {
		    echo '<p class="bg-danger">'.$e->getMessage().'</p>';
		    return false;
		}
		return true;
	}

}


?>
