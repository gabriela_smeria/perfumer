
<?php
include 'includes/head.php';
include 'includes/pagetop.php';

$currentUser = $_SESSION['user'];

if (!empty($_GET)){
	if(!empty($_GET['judet']) && !empty($_GET['nume_dest']) && !empty($_GET['prenume_dest']) && !empty($_GET['oras']) && !empty($_GET['adresa']) && !empty($_GET['telefon'])){
		if(isset($_GET['codp']) && (strlen($_GET['codp']) === 6) && (intval($_GET['codp']) !== 0)){
			if(strlen($_GET['telefon']) < 10 || intval($_GET['telefon']) === 0){
				echo '<p class="error-msg-tel">Numar de telefon invalid!</p>';
			}else{
				$addressString = "DESTINATAR: "+$_GET['prenume_dest']+" "+$_GET['nume_dest']+" JUDET: "+$_GET['judet']+" ORAS: "+$_GET['oras']+" CODPOSTAL: "+$_GET['codp']+" ADRESA: "+$_GET['adresa']+" TELEFON: "+$_GET['telefon'];
				$stmt = $db->prepare('INSERT INTO orders (username, dateplaced, address) VALUES (:username, SYSDATE(), :addressStr) ');
				$stmt->bindParam(':username',$currentUser,PDO::PARAM_STR);
				$stmt->bindParam(':addressStr',$addressString,PDO::PARAM_STR);
				$stmt->execute();

				$stmt = $db->prepare('SELECT LAST_INSERT_ID()');
				$stmt->execute();
				$row = $stmt->fetch(PDO::FETCH_NUM);

				$orderid = $row[0];

				$stmt = $db->prepare('UPDATE products SET quantity = quantity - coalesce((SELECT quantity FROM orderlines WHERE username=:username AND productid=products.productID AND orderid=-1),0)');
				$stmt->bindParam(':username',$currentUser,PDO::PARAM_STR);
				$stmt->execute();

				$stmt = $db->prepare('UPDATE orderlines SET orderid = :orderid WHERE username = :username AND orderid = -1');
				$stmt->bindParam(':username',$currentUser,PDO::PARAM_STR);
				$stmt->bindParam(':orderid',$orderid,PDO::PARAM_INT);
				$stmt->execute();

				$stmt = $db->prepare('SELECT email FROM members WHERE username = :username');
				$stmt->bindParam(':username',$currentUser,PDO::PARAM_STR);
				$stmt->execute();

				$row = $stmt->fetch(PDO::FETCH_ASSOC);

				$to = $row['email'];
				$subject = "Finalizare comanda";
				$body = "Comanda cu numarul ".$orderid." a fost inregistrata. Status curent: plasata.";
				$additionalheaders = "From: <".SITEEMAIL.">\r\n";
				$additionalheaders .= "Reply-To: ".SITEEMAIL;
				mail($to, $subject, $body, $additionalheaders);
				header('Location: reg_order.php');
			}
			//echo "<p>Comanda a fost inregistrata. Veti primi un email ...</p>";
		}else{
			echo '<p class="err_msg">Cod postal invalid!</p>';
		}
	}else{
		echo '<p class="err_msg1">Toate campurile sunt obligatorii!</p>';
	}
}
?>

<div id="site"><div id="content">
	<form class="formular"><br>
		<fieldset>
			<center>
					<legend> Informatii generale </legend><br>
			</center>

			<label class="opt">&#10048; Nume: </label><input name="nume_dest" type="text" /><br />
			<label class="opt">&#10048; Prenume: </label><input name="prenume_dest" type="text" required /> <br><br />
		</fieldset>

		<fieldset>
			<center>
				<legend>Informatii suplimentare</legend>
			</center><br>

			<label class="opt">&#10048; Judet: </label><select name="judet" required >
                    <option value="0">Alege judet</option>
                    <option value="Alba">Alba</option>
                    <option value="Arad">Arad</option>
                    <option value="Arges">Arges</option>
                    <option value="Bacau">Bacau</option>
                    <option value="Bihor">Bihor</option>
                    <option value="Bistrita Nasaud">Bistrita Nasaud</option>
                    <option value="Botosani">Botosani</option>
                    <option value="Brasov">Brasov</option>
                    <option value="Braila">Braila</option>
                    <option value="Bucuresti">Bucuresti</option>
                    <option value="Buzau">Buzau</option>
                    <option value="Caras Severin">Caras Severin</option>
                    <option value="Calarasi">Calarasi</option>
                    <option value="Cluj">Cluj</option>
                    <option value="Constanta">Constanta</option>
                    <option value="Covasna">Covasna</option>
                    <option value="Dambovita">Dambovita</option>
                    <option value="Dolj">Dolj</option>
                    <option value="Galati">Galati</option>
                    <option value="Giurgiu">Giurgiu</option>
                    <option value="Gorj">Gorj</option>
                    <option value="Harghita">Harghita</option>
                    <option value="Hunedoara">Hunedoara</option>
                    <option value="Ialomita">Ialomita</option>
                    <option value="Iasi">Iasi</option>
                    <option value="Ilfov">Ilfov</option>
                    <option value="Maramures">Maramures</option>
                    <option value="Mehedinti">Mehedinti</option>
                    <option value="Mures">Mures</option>
                    <option value="Neamt">Neamt</option>
                    <option value="Olt">Olt</option>
                    <option value="Prahova">Prahova</option>
                    <option value="Satu Mare">Satu Mare</option>
                    <option value="Salaj">Salaj</option>
                    <option value="Sibiu">Sibiu</option>
                    <option value="Suceava">Suceava</option>
                    <option value="Teleorman">Teleorman</option>
                    <option value="Timis">Timis</option>
                    <option value="Tulcea">Tulcea</option>
                    <option value="Vaslui">Vaslui</option>
                    <option value="Valcea">Valcea</option>
                    <option value="Vrancea">Vrancea</option>
   				 	</select>  <br />
			<label class="opt">&#10048; Oras: </label><input name="oras" type="text" required /> <br />
			<label class="opt">&#10048; Cod postal: </label><input name="codp" type="text" required /> <br />
			<label class="opt">&#10048; Telefon: </label><input name="telefon" type="text" required /><br />
			<label class="opt">&#10048; Adresa: </label><br><div class="other"><textarea name="adresa" cols="39" rows="5" required> </textarea></div> <br />

		</fieldset><br>
	 <input type="submit" value="Trimite comanda" />
	</form>
</div></div>
<?php
include 'includes/pagebottom.php';
?>
