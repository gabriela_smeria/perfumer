<?php
	include 'includes/head.php';
	include 'includes/pagetop.php';

	$currentUser = $_SESSION['user'];
?>
<div id="site">
<div id="cart-content">
<h1>Cosul meu</h1>
<?php
	if(isset($_GET['deleteproduct'])){
		$productIdForDelete = $_GET['deleteproduct'];
		if(!$cos->removeProduct($currentUser,$productIdForDelete)){
			echo '<p>Produsul nu a putut fi sters din cos!</p>';
		}
	}

	if(isset($_POST['quantity']) && isset($_POST['productid'])){
		$qu = $_POST['quantity'];
		$productId = $_POST['productid'];
		if(!$cos->changeProduct($currentUser,$productId,$qu)){
			echo '<p>Produsul nu a putut fi modificat!</p>';
		}
	}

	if(isset($_POST['emptycart'])){
		if(!$cos->emptyContent($currentUser)){
			echo '<p>Cosul nu a putut fi golit!</p>';
		}
	}

	$productsInCart = $cos->getContentForUser($currentUser);

?>

<div id="products"><ul>
	<?php
	$totalCart = 0;
		foreach ($productsInCart as $product) {
			echo '<li class="cart-item">';

								echo '<a href="perfumepage.php?productID='.$product['productID'].'">';
								if($product['promo'] > 0){
									echo '<span class="new newC"><img src="'.$siteroot.'/images/elements/sale.png" alt="Promotii" width=70 heigth=70></span>';
								}

									echo '<img src="'.$siteroot.'/images/perfumes/'.$product['brand'].'/'.$product['image'].'.jpg" alt="Perfumer" width=70 heigth=70>';
									echo '</a>';
									echo '<span class="name nameC">'.$product['name'].'</span>';
									echo '<span class="brand brandC">by '.$product['brand'].'</span>';
									echo '<form class="quantity quantityC"><input name="productid" value="'.$product['productID'].'" hidden />';
									echo 'Cantitate: <input name="quantity" type="number" min="1" max="100" value="'.$product['quantity'].'" width=40 />';
									echo '<input type="submit" value="Modifica" formmethod="POST"/>';
									echo '</form>';
									echo '<a class="delete" href="'.$siteroot.'/shopping_cart.php?deleteproduct='.$product['productID'].'">Sterge</a>';

									$price = 0;
									if($product['promo'] > 0){
										$newprice = $product['price'] - $product['price']*$product['promo']/100;
										$price = ceil($newprice);
									}
									else{
										$price = $product['price'];
									}
									echo '<span class="price priceC">'.$price.' RON</span>';
									$totalItem = $price * $product['quantity'];
									$totalCart += $totalItem;
									echo '<span class="total-price-item total-price-itemC"> Total: '.$totalItem.' RON</span>';
								echo '</li>';
		}
	?>
</ul>
<span class="total-price-cart">Total cos: <?php echo $totalCart; ?> RON<?php if ($totalCart > 0){
	echo '<a href="'.$siteroot.'/finalize_order.php"><button>Finalizare comanda</button></a>';
	}
?></span>
<?php
if($totalCart > 0){
		echo '<form class="empty-cart-form"><input type="text" name="emptycart" hidden /><input type="submit" value="Goleste cos" formmethod="POST"/></form>';

}
?>

</div></div></div>
<?php include 'includes/pagebottom.php';?>
