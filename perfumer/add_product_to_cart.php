<?php
include "config/config.php";

header('Content-Type: text/xml');
$siteroot = '/perfumer';

$productId = $_GET['productid'];
$quantity = 1;
if(isset($_GET['quantity'])){
	$quantity = $_GET['quantity'];
}

$currentUser = $_SESSION['user'];
$success = 0;
if($cos->addProduct($currentUser,$productId,$quantity)){
	$success = 1;
}

echo '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>';
echo '<response>';
echo "<added>".$success."</added>";
echo "</response>";
?>
