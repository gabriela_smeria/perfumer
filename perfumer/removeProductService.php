<?php
include 'config/config.php';
$deletedProduct = $_GET['deletedProductID'];
function remove($removable) {
    global $db;
    $query=$db->prepare("DELETE from `products` WHERE `productID`=?");
    $query->bindValue(1,$removable);
    $query->execute();
}

remove($deletedProduct);
?>
